"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.apiService = void 0;
var request = __importStar(require("superagent"));
var index_1 = require("../@constants/index");
/**
 *  API services:
 */
var users = "users";
var dishes = "dishes";
var orders = "orders";
exports.apiService = {
    users: {
        getOne: function (id) { return request.get(index_1.API + "/" + users + "/" + id); },
        getAll: function () { return request.get(index_1.API + "/" + users); },
        create: function (user) { return request.post(index_1.API + "/" + users).send(user); }
    },
    dishes: {
        getOne: function (id) { return request.get(index_1.API + "/" + dishes + "/" + id); },
        getAll: function () { return request.get(index_1.API + "/" + dishes); },
        create: function (dish) { return request.post(index_1.API + "/" + orders).send(dish); }
    },
    orders: {
        getOne: function (id) { return request.get(index_1.API + "/" + orders + "/" + id); },
        getAll: function () { return request.get(index_1.API + "/" + orders); },
        create: function (order) { return request.post(index_1.API + "/" + orders).send(order); }
    }
};
