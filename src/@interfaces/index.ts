export interface IUser {
    id?: number
    name: string
    isAdmin: boolean
    balance: number
    alergicTo: Array<string>
    isVegan: boolean
    isVegetarian: boolean
}

export interface IDish {
    id?: number
    name: string
    cost: number
    isVegan: boolean
    isVegetarian: boolean
    ingredients: Array<string>
}

export interface IOrder {
    id?: number
    userId: number
    dishId: number
    dishes: Array<number>
    total: number
    fee: number
    dateTime: string
}