import { apiService } from './api'
import { IUser, IDish } from '../@interfaces'


export default class Order {
    async create(user: IUser): Promise<any> {
        return apiService.users.create(user)
    }

    async get(id: number): Promise<any> {
        return apiService.users.getOne(id)
    }

    checkOrder() {

    }

}