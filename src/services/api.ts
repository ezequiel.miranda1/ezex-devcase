import * as request from 'superagent'
import { API } from '../@constants/index'
import { IUser, IDish, IOrder } from '../@interfaces/'

/**
 *  API services:
 */

const users = `users`
const dishes = `dishes`
const orders = `orders`

export const apiService = {
    users: {
        getOne: (id: any) => request.get(`${API}/${users}/${id}`),
        getAll: () => request.get(`${API}/${users}`),
        create: (user: IUser) => request.post(`${API}/${users}`).send(user)
    },
    dishes: {
        getOne: (id: any) => request.get(`${API}/${dishes}/${id}`),
        getAll: () => request.get(`${API}/${dishes}`),
        create: (dish: IDish) => request.post(`${API}/${orders}`).send(dish)

    },
    orders: {
        getOne: (id: any) => request.get(`${API}/${orders}/${id}`),
        getAll: () => request.get(`${API}/${orders}`),
        create: (order: IOrder) => request.post(`${API}/${orders}`).send(order)
    }
}