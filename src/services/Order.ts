import { apiService } from '../services/api'
import { IUser, IDish, IOrder } from '../@interfaces/'


export default class Order {
    private user: IUser | undefined = undefined
    private orderList: Array<IDish> = []

    constructor(user: IUser) {
        this.user = user
    }

    async create(order: IOrder, user: IUser, dish: IDish): Promise<any> {

        // 1- Check if the order it's ok
        // 2- Create order
        // 3- Return new order
        return await apiService.orders.create(order)
    }

    addToOrder(idDish: IDish) {
        // Check if the product is sutiable for the user
        const isTrue = true

        if (isTrue) {
            this.orderList = [...this.orderList, idDish]
        }

    }

    checkOrder() {
        console.log(this.user)
        console.log(this.user)
    }

}