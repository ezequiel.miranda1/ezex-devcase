import { apiService } from './api'
import { IUser, IDish } from '../@interfaces'


export default class Order {
    async create(dish: IDish): Promise<any> {
        return await apiService.dishes.create(dish)
    }
}