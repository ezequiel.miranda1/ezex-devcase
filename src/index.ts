import Order from './services/Order'
import User from './services/User'

/** 2 - As developer, I want to create a service for manage orders (core js)
 *  
 */

const currentUserId = 1;

const newOrder = {
    "userId": 1,
    "dishId": 2,
    "dishes": [1, 2],
    "total": 20,
    "fee": 2,
    "dateTime": new Date().toString()
};

const dish = {
    "name": "Some delicius dish",
    "cost": 10,
    "isVegan": true,
    "isVegetarian": true,
    "ingredients": [
        "potato",
        "onion"
    ]
}

    ; (async () => {
        try {
            // Get current user
            const user = new User()
            const currentUser = await user.get(currentUserId)

            // Crete new order

            const myOrder = new Order(currentUser)
            myOrder.addToOrder(dish)
            const result = await myOrder.create(newOrder, currentUser, dish)

            console.log(result.body)
        } catch (err) {
            console.error(err);
        }
    })();



/**  As developer, I want to create a service for manage users
 *
 */

// (async () => {
//     try {
//         const myUser = new User()
//         const result = await myUser.get(currentUserId)
//         console.log(result.body)
//     } catch (err) {
//         console.error(err);
//     }
// })();
