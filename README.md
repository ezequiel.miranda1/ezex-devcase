# Another dev case

This current dev case is WIP

## Init application

1 - Start db: `npm run start:db`

2 - Run input file (src/index.ts): `npm run start`

## Contexts:

We have to develop a front-end application for order food online, we were provided with an API with business data.

Database with `https://github.com/typicode/json-server`

```
  http://localhost:3004/users
  http://localhost:3004/dishes
  http://localhost:3004/orders
```

## Description

1 - As a developer, I want to create `util` for `filter` data with different conditions. `(core js)`

2 - As developer, I want to create a `service` for manage `orders` `(core js)`

5 - As developer, I want to design a responsive `layout` for the web application. `(Layout Design)`

4 - As developer, I want to create a front-end application using some framework and .... `(Framework)`

5 - As developer, I want to secure some routes `(Security)`

---

## Tasks Detail:

### 1 - Filter Service Detail:

Create util for `filter` data with different conditions.

```js
    type Filter(condition: function, arr:[])
}
```

---

### 2 - Order Service detail: WIP

Create branch `feature/order-service`

The core of the service is to create orders on the `db`, but the order should be validated before being created.

**Pre conditions:**

- get any oject user from API GET `http://localhost:3004/user/:id`
- get any object food froom api GET `http://localhost:3004/dishes/`

The order service should validate if the dish is ok for the user (check vegan, vegetarian, allergic stuff) and also the money balance. If the order is ok also rest the balance and create the order on the database.

When the user try to create a new order, the order should validate with the `user object`.

```js
    interface Order{
        create( theUser: user, newOrder: order)
        validateOrder() // Validate order with the defferentes conditions, user properties and money ballance.
        getDesiredFood() // Based on the user properties
    }
```

---

### 3 - Layout detail:

    TBC

---

### 4 - Front-end Framework implementation

Init application using your favorite framework.

---

### 5 - Secured routes

Implement route security using specific framework utils, token, etc

- `localhost:3000/`(Not secure)
- `localhost:3000/secure` : (Secure, only admin user)

---

## Another features we can add

- Check order and balance using different currencies
  - https://exchangeratesapi.io/

## Starter project used:

https://github.com/stemmlerjs/simple-typescript-starter
